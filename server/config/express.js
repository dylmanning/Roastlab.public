'use strict';

var express = require('express');
var session = require('express-session');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var passport = require('passport')
var localStrategy = require('passport-local').Strategy;
var User = require('../models/user/user.model');

module.exports = function(app) {

  app.use('/', express.static(path.join(__dirname + '/../../client')));

  app.use(session({
    secret: 'roastlab-session',
    resave: true,
    saveUninitialized: true
  }));

  app.use(bodyParser.urlencoded({
    extended: true
  }));

  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(passport.initialize());
  app.use(passport.session());

  passport.use(new localStrategy(
    function(username, password, done) {
      User.findOne({
        username: username,
        password: password
      }, function(err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, { msg: 'Invalid Email.'});
        }
        return done(null, user);
      });
    }));

  passport.serializeUser(function(user, done) {
    done(null, user);
  });
  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

};
