'use strict';

var User = require('../models/user/user.model');
var Lot = require('../models/lot/lot.model');

//create admin user
User.find({}).remove(function() {
  User.create({
    username: 'admin',
    password: 'password',
    email: 'admin@roastlab.beta',
    firstName: 'Dylan',
    lastName: 'Manning',
    joined: new Date()
  });
  //create Mac's account
  User.create({
    username: 'mac',
    password: '058471',
    email: 'mac@smallbatch.com.au',
    firstName: 'Macaulay',
    lastName: 'Dixon',
    joined: new Date()
  });
});


// Lot.find({}).remove(function() {
//   Lot.create({
//     name: 'El Pilar',
//     rating: '5',
//     assignment: 'Espresso',
//     origin: 'Kenya',
//     farmer: 'Tom Jones',
//     addedBy: 'Dylan',
//     state: '4',
//     metrics: {
//       cropId: 'CROP23423',
//       lotNo: '343',
//       lotSz: '23',
//       bCode: '433'
//     },
//     assesments: [{ name: 'Arrival', url: 'views/partials/arrival.view.html' }, { name: 'Green', url: 'views/partials/green.view.html'}]
//   }, function(err) {
//       if (err) { return console.error(err); }
//   });
// });
