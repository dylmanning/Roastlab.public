'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var lotSchema = new Schema({
  name: String,
  rating: { type: Number, default: 0 },
  assignment: String,
  origin: String,
  country: String,
  farmer: String,
  addedBy: String,
  added: Date,
  updated: { type: Date, default: new Date() },
  updatedBy: String,
  state: { type: Number, default: 0 },
  status: String,
  group: String,
  visits: [{ start: Date, end: Date }],
  info: {
    connection: String,
    history: String,
    production: String,
    climate: String,
    comments: [{ user: String, date: Date, comment: String }]
  },
  metrics: {
    cropId: String,
    lotSz: String,
    bCode: String,
    inventory: Number,
  },
  geo: {
    lat: Number,
    lng: Number,
    altitude: Number,
  },
  assesments : [{
    name: String,
    view: String,
    edit: String,
    added: Date,
    addedBy: String,
    TDS: Number,
    EXT: Number,
    prCode: String,
    cupDate: Date,
    roastDate: Date,
    roastedBy: String,
    roastEquipment: String,
    dry: String,
    wet: String,
    sweet: String,
    acid: String,
    comments: String,
    initial: {
      component: String,
      TDS: Number,
      EXT: Number,
      dose: Number,
      yield: Number,
      time: Number,
      fruit: String,
      acid: String,
      finish: String,
      tactility: String,
      balance: String,
      additional: String,
      milk: String
    }
  }],
  production: [{
    name: String,
    url: String,
    addded: Date,
    addedBy: String
  }]
});

module.exports = mongoose.model('Lot', lotSchema);
