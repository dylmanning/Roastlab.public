(function(){
'use strict';

var loginService = function($q, $http, userConstant) {

  var srv = this;

  srv.login = function(user) {
    return $http.post('/api/login', user)
      .catch(function(err) {
        if(err.status == 400) {
          return {type: 'warning', msg: 'Incomplete, please try again.'};
        } else if(err.status == 401) {
          return {type: 'danger', msg: 'Incorrect email/password, please try again.'};
        }
      });
  };

  srv.logout = function() {
    return $http.post('/api/logout')
      .success(function(res) {
        for(var i in userConstant) {
          userConstant[i] = '';
        }
      });
  };

  srv.check = function() {
    return $http.get('/api/loggedin')
      .success(function(res) {
        if(res !== '0' && isEmpty(userConstant)) {
          userConstant.firstName = res.firstName;
          userConstant.lastName = res.lastName;
          userConstant.email = res.email;
          userConstant.username = res.username;
        }
      });
  };

  var isEmpty = function(obj) {
    for(var i in obj) {
      if(obj[i] != '') {
        return false;
      }
      return true;
    }
  };

};

angular
  .module('roastlab')
  .service('loginService', [
    '$q',
    '$http',
    'userConstant',
    loginService
  ]);

})();
