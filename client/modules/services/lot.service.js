(function(){
'use strict';

var lotService = function($q, $http, geoService) {

  var srv = this;

  srv.getOne = function(id) { return $http.get('/api/lot/' + id); };
  srv.updateOne = function(id, data) { return $http.put('/api/lot/' + id, data); };

  srv.get = function() {
    return $http.get('/api/lots');
  };

  srv.remove = function(id) {
    return $http.delete('/api/lot/' + id);
  };

  srv.create = function(obj) {
    var d = $q.defer();
    geoService.geocode(obj.origin)
      .then(function(res) {
        obj.lat = res.lat;
        obj.lng = res.lng;
        obj.origin = res.origin;
        obj.country = res.country;
        geoService.elevation(obj.lat, obj.lng)
        .then(function(res) {
          obj.altitude = res.altitude;
          $http.post('/api/lot', obj)
          .success(function(res) {
            d.resolve(res);
          });
        });
      });
    return d.promise;
  };

};

angular
  .module('roastlab')
  .service('lotService', [
    '$q',
    '$http',
    'geoService',
    lotService
  ]);

})();
