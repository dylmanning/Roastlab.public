(function() {
  'use strict';

  var emailService = function($q, $http) {

    var srv = this;

    srv.sendFeedback = function(obj) {
      return $http.post('/api/feedback', obj)
        .success(function(res) {
          return res;
        });
    }

  };

  angular
    .module('roastlab')
    .service('emailService', [
      '$q',
      '$http',
      emailService
    ]);

})();
