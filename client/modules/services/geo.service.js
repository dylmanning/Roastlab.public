(function() {
  'use strict';

  var geoService = function($q) {

    var srv = this;

    srv.geocode = function(location) {
      var geocoder = new google.maps.Geocoder();
      var d = $q.defer();
      setTimeout(function() {
        geocoder.geocode({
          'address': location
        }, function(res) {
          var obj = {};
          for (var i = 0; i < res.length; i++) {
            obj.lat = res[i].geometry.location.lat();
            obj.lng = res[i].geometry.location.lng();
            obj.origin = res[i].formatted_address;
            for (var k = 0; k < res[i].address_components.length; k++) {
              if (res[i].address_components[k].types[0] == 'country')
                obj.country = res[i].address_components[k].long_name;
            }
          }
          d.resolve(obj);
        });
      }, 100);
      return d.promise;
    }

    srv.elevation = function(lat, lng) {
      var location = [{ lat: lat, lng: lng }];
      var elevator = new google.maps.ElevationService();
      var d = $q.defer();
      setTimeout(function() {
        elevator.getElevationForLocations({
          'locations': location
        }, function(res) {
          var obj = {};
          for (var i = 0; i < res.length; i++) {
            obj.altitude = res[i].elevation;
          }
          d.resolve(obj);
        });
      }, 100);
      return d.promise;
    }

  };

  angular
    .module('roastlab')
    .service('geoService', [
      '$q',
      geoService
    ]);

})();
