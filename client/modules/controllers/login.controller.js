(function() {
  'use strict';

  var LoginController = function($state, $timeout, userConstant, loginService) {

    var vm = this;

    vm.alerts = [];
    vm.alert = false;

    vm.toggleAlert = function() {
      vm.alert = !vm.alert;
    }

    vm.closeAlert = function(index) {
      vm.alerts.splice(index, 1);
    };

    vm.login = function() {
      var user = { username: vm.username, password: vm.password };
      loginService.login(user)
        .then(function(res) {
          if(res.status == 200) {
            $state.go('dashboard');
          } else {
            vm.toggleAlert();
            vm.alerts.push(res);
            $timeout(function() {
              vm.toggleAlert();
              $timeout(function() {
              vm.alerts = [];
              }, 500);
            }, 5000)
          }
        });
    };

  };

  var LoginConfig = function($stateProvider) {

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.view.html',
        controllerAs: 'vm',
        controller: 'LoginController'
      });

  };

  angular
    .module('roastlab.login', [])

  .config([
    '$stateProvider',
    LoginConfig
  ])

  .controller('LoginController', [
    '$state',
    '$timeout',
    'userConstant',
    'loginService',
    LoginController
  ]);

})();
