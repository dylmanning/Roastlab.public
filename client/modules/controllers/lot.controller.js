(function() {
  'use strict';

  var LotController = function($state, userConstant, lotService, loginService, NgMap, lot) {

    var vm = this;

    vm.date = new Date();
    vm.editing = false;

    vm.render = function(obj) {
      vm.lot = obj;
      vm.assesments = obj.assesments;
      vm.visits = obj.visits;
    };

    vm.home = function() {
      $state.go('dashboard');
    };

    vm.edit = function() {
      vm.editing = !vm.editing;
    };

    vm.remove = function(id) {
      lotService.remove(id)
        .then(function(res) {
          $state.go('dashboard');
          //push alert
        });
    };

    vm.update = function(obj) {
      lotService.updateOne(obj._id, obj)
        .then(function(res) {
          lotService.getOne(obj._id)
            .then(function(res) {
              vm.render(res.data);
              vm.edit();
              //push alert
            });
        });
    };

    vm.addVisit = function(start, end) {
      vm.visits.push({ start: start, end: end });
    };

    vm.removeVisit = function(index) {
      vm.visits.splice(index, 1);
    };

    vm.addAssesment = function() {
      vm.assesments.push({name: 'Unassigned', view: 'views/partials/assesment.view.html', edit: 'views/partials/assesment.edit.html'});
      vm.lot.state++;
      //push alert
    };

    vm.removeAssesment = function(index) {
      vm.assesments.splice(index, 1);
      vm.lot.assesments = vm.assesments;
      vm.lot.state--;
      lotService.updateOne(vm.lot._id, vm.lot)
        .then(function(res) {
          lotService.getOne(vm.lot._id)
            .then(function(res) {
              $state.reload();
              //push alert
            });
        });
    };

    vm.updateAssesment = function(obj, index) {
      vm.lot.assesments.splice(index, 1, obj);
      lotService.updateOne(vm.lot._id, vm.lot)
        .then(function(res) {
          lotService.getOne(vm.lot._id)
            .then(function(res) {
              vm.render(res.data)
              vm.edit();
              //push alert
            });
        });
    };

    vm.drawMap = function() {
      NgMap.getMap({id: 'map'})
      .then(function(map) {
        var center = new google.maps.LatLng(vm.lot.geo.lat, vm.lot.geo.lng);
        var avocado = [{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#aee2e0"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#abce83"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#769E72"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#7B8758"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#EBF4A4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#8dab68"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5B5B3F"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ABCE83"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#A4C67D"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#9BBF72"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#EBF4A4"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#87ae79"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#7f2200"},{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":4.1}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#495421"}]},{"featureType":"administrative.neighborhood","elementType":"labels","stylers":[{"visibility":"off"}]}];
        map.setCenter(center);
        map.setOptions({scrollwheel: false, draggable: false, zoom: 7, disableDefaultUI: true, styles: avocado})
        var marker = new google.maps.Marker({
          position: center,
          map: map
        });
      });
    };

    vm.isEmptyItem = function(obj) {
      if(obj === null || obj === undefined) {
        return true;
      }
      return Object.keys(obj).length === 0;
    };

    vm.isEmptyArray = function(obj) {
      if(obj.length > 0) {
        return false;
      } else {
        return true;
      }
    }

    vm.render(lot);
    vm.drawMap();

  };

  var LotConfig = function($stateProvider) {

    $stateProvider
      .state('lot', {
        url: '/:id',
        templateUrl: 'views/lot.view.html',
        controllerAs: 'vm',
        controller: 'LotController',
        resolve: {
          authenticate: function($q, $state, loginService) {
            var d = $q.defer();
            return loginService.check()
              .then(function(res) {
                if (res.data !== '0') {
                  d.resolve();
                } else {
                  d.reject();
                  $state.go('login');
                }
              });
            return d.promise;
          },
          lot: function(lotService, $stateParams) {
            return lotService.getOne($stateParams.id)
              .then(function(res) {
                return res.data;
              });
          }
        }
      });

  };

  angular
    .module('roastlab.lot', [])

  .config([
    '$stateProvider',
    LotConfig
  ])

  .controller('LotController', [
    '$state',
    'userConstant',
    'lotService',
    'loginService',
    'NgMap',
    'lot',
    LotController
  ]);

})();
