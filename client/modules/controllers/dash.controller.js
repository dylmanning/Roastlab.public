(function() {
  'use strict';

  var DashboardController = function($state, userConstant, lotService, lots) {

    var vm = this;

    vm.lot = {};
    vm.lots = lots;
    vm.date = new Date();
    vm.user = userConstant.firstName;
    vm.adding = false;
    vm.creating = false;

    vm.add = function() {
      vm.adding = !vm.adding;
    }

    vm.create = function(obj) {
      vm.creating = true;
      obj.added = vm.date;
      obj.addedBy = vm.user;
      lotService.create(obj)
        .then(function(res) {
          vm.get();
          vm.add();
          vm.creating = false;
          vm.lot = {};
          //push alert
        });
    };

    vm.get = function() {
      lotService.get()
        .then(function(res) {
          vm.lots = res.data;
        });
    };

  };

  var DashboardConfig = function($stateProvider) {

    $stateProvider
      .state('dashboard', {
        url: '/',
        templateUrl: 'views/dashboard.view.html',
        controllerAs: 'vm',
        controller: 'DashboardController',
        resolve: {
          authenticate: function($q, $state, loginService) {
            var d = $q.defer();
            return loginService.check()
              .then(function(res) {
                if (res.data !== '0') {
                  d.resolve();
                } else {
                  d.reject();
                  $state.go('login');
                }
              });
            return d.promise;
          },
          lots: function(lotService) {
            return lotService.get()
              .then(function(res) {
                return res.data;
              });
          }
        }
      });

  };

  angular
    .module('roastlab.dashboard', [])

  .config([
    '$stateProvider',
    DashboardConfig
  ])

  .controller('DashboardController', [
    '$state',
    'userConstant',
    'lotService',
    'lots',
    DashboardController
  ]);

})();
