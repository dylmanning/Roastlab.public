(function() {
  'use strict';

  var tabs = function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: true,
      controller: ['$scope', function($scope) {
        var panes = $scope.panes = [];

        $scope.select = function(pane, index) {
          $scope.$emit('select', index);
          angular.forEach(panes, function(pane) {
            pane.selected = false;
          });
          pane.selected = true;
        }

        this.addPane = function(pane) {
          if (panes.length == 0) $scope.select(pane);
          panes.push(pane);
        }

      }],
      template: '<div class="tabbable">' +
                  '<ul class="nav nav-pills lot-tabs">' +
                    '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">' +
                      '<button class="btn btn-sm btn-default" href="" ng-click="select(pane, $index)">{{pane.title}}</button>' +
                    '</li>' +
                  '</ul>' +
                  '<div class="tab-content" ng-transclude></div>' +
                '</div>',
      replace: true
    };
  };

  var panes = function() {
    return {
      require: '^tabs',
      restrict: 'E',
      transclude: true,
      scope: {
        title: '@'
      },
      link: function(scope, element, attrs, tabsCtrl) {
        tabsCtrl.addPane(scope);
      },
      template: '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
                '</div>',
      replace: true
    };
  };

  angular
    .module('roastlab')
    .directive('tabs', tabs)
    .directive('panes', panes);

})();
