(function() {
  'use strict';

  angular
    .module('roastlab')
    .constant('userConstant', {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      joined: '',
      lastLogin: '',
    });

})();
