(function(){
  'use strict';

  var label = function(){
    return function(state) {
      switch(state) {
        case 1:
          return'label label-success label-green';
          break;
        case 2:
          return 'label label-primary label-arrival';
          break;
        case 3:
          return 'label label-warning label-initial';
          break;
        case 4:
          return 'label label-danger label-production';
          break;
        default:
          return 'label label-default label-new';
      }
    };
  };

angular
  .module('roastlab')
  .filter('label', label);
  
})();
