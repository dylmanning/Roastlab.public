(function(){
  'use strict';

  var dateFilter = function($filter) {
    return function(date) {
      var system_date = new Date();
      var diff = Math.floor((Date.parse(system_date) - Date.parse(date)) / 1000);
      if (diff <= 90) return '1 minute ago';
      if (diff <= 3540) return Math.round(diff / 60) + ' minutes ago';
      if (diff <= 5400) return '1 hour ago';
      if (diff <= 86400) return Math.round(diff / 3600) + ' hours ago';
      if (diff <= 129600) return '1 day ago';
      if (diff < 604800) return Math.round(diff / 86400) + ' days ago';
      else return $filter('date')(new Date(date), 'MM/dd/yyyy');
    };
  };

  var timeFilter = function($filter) {
    return function(date) {
      var system_date = new Date();
      var diff = Math.floor((Date.parse(system_date) - Date.parse(date)) / 1000);
      if (diff <= 86400) return $filter('date')(new Date(date), 'h:mma');
      if (diff < 604800) return $filter('date')(new Date(date), 'EEEE');
      else return $filter('date')(new Date(date), 'MM/dd/yyyy');
    };
  };

angular
  .module('roastlab')
  .filter('dateFilter', dateFilter)
  .filter('timeFilter', timeFilter);

})();
