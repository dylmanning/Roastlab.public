(function() {
  'use strict';

  var GlobalController = function($state, $scope, userConstant, emailService, loginService) {

    var vm = this;

    $scope.$on('$stateChangeSuccess', function() {
      $scope.user = userConstant.firstName;
    });

    vm.logout = function() {
      loginService.logout()
        .then(function() {
          $state.go('login');
        });
    };

    vm.feedback = function(obj) {
      obj.user = userConstant.firstName;
      emailService.sendFeedback(obj)
        .then(function(res) {
          //push alert
        });
    };

  };

  var GlobalConfig = function($urlRouterProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/login');
    $httpProvider.interceptors.push('loadingService');

  };

  angular
    .module('roastlab', [
      'ui.router',
      'ui.bootstrap',
      'ngMap',
      'ngAnimate',
      'roastlab.login',
      'roastlab.dashboard',
      'roastlab.lot'
    ])

  .config([
      '$urlRouterProvider',
      '$httpProvider',
      GlobalConfig
    ])

    .controller('GlobalController', [
      '$state',
      '$scope',
      'userConstant',
      'emailService',
      'loginService',
      GlobalController
    ]);

})();
